//all plants with their tags
SELECT name, createdAt, OUT().name as tags from Plant
//all plants that belongs to tag
SELECT expand(in("E")) From Tag WHERE id="02008980-6842-40c8-89a1-46a0abd4e9ee";
//all tags on plant
SELECT expand(out("E")) From Plant WHERE id="b18a13c9-2a50-4869-ac6f-398544cb33da";
//all plants who match some tag with all their tags
SELECT name, OUT().name as tags from (SELECT expand(in()) from Tag where id="0a459cc7-2c02-4c27-9497-a4843aef3a0c");