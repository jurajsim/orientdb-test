const process = require('process');
const OrientDb = require("orientjs").Db;
const OrientDBClient = require("orientjs").OrientDBClient;
const uuid = require('uuid');
const randomstring = require("randomstring");
const _ = require('lodash');


const TAGS_COUNT = 100;
const CATEGORIES_COUNT = 100;
const PLANTS_COUNT = 10000;

const Batch = class {
    constructor() {
        this.queue = [];
    }

    add(fn) {
        this.queue.push(fn);
    }

    async run(concurency = 5) {
        let batch = [];
        while (this.queue.length > 0) {
            for (let i = 0; i < concurency; i++) {
                if (this.queue.length <= 0) {
                    break;
                }
                let fn = this.queue.pop();
                batch.push(fn());
            }
            await Promise.all(batch)
        }
    }
};

const run = async function () {

    let client = await OrientDBClient.connect({
                                                  host: "172.20.0.88",
                                                  port: 2424
                                              });

    let pool = await client.sessions({name: "testdb", username: "root", password: "root", pool: {max: 10}});
    console.log('Got pool');

    let session = await pool.acquire();
    console.log('Got session');

    await session.command('TRUNCATE CLASS Tag UNSAFE;').all();
    await session.command('TRUNCATE CLASS Plant UNSAFE;').all();
    await session.command('TRUNCATE CLASS Category UNSAFE;').all();
    await session.command('TRUNCATE Class E UNSAFE;').all();

    console.time('insert categories');
    const categories = [];
    for (let i = 0; i < CATEGORIES_COUNT; i++) {
        let category = {
            name     : randomstring.generate(20),
            id       : uuid(),
            createdAt: new Date()
        };
        categories.push(category.id);
        let result = await session.command(`INSERT
        INTO
          Category
          CONTENT ${JSON.stringify(category)}`).all();
        console.log(result);
    }
    console.timeEnd('insert categories');

    //insert random tags
    console.time('insert tags');
    const tags = [];
    for (let i = 0; i < TAGS_COUNT; i++) {
        let tag = {
            name     : randomstring.generate(10),
            id       : uuid(),
            createdAt: new Date()
        };
        tags.push(tag.id);
        let result = await session.command(`INSERT
        INTO
          Tag
          CONTENT ${JSON.stringify(tag)}`).all();
        console.log(result);
    }
    console.timeEnd('insert tags');

    console.time('insert posts');
    for (let i = 0; i < PLANTS_COUNT; i++) {
        let plant = {
            name     : randomstring.generate(80),
            id       : uuid(),
            aliases  : {
                cs: 'aaa',
                sk: 'bbb',
                de: 'sss'
            },
            createdAt: new Date()
        };
        let result = await session.command(`INSERT
        INTO
          Plant
          CONTENT ${JSON.stringify(plant)}`).all();

        let tagsClone = tags.slice(0);
        //console.log(tags, '=========', tagsClone);
        let tagsCount = _.random(5, 18);
        let localTagIds = [];
        for (let j = 0; j < tagsCount; j++) {
            let idx = _.random(0, tagsClone.length - 1);
            // console.log('Random ', idx);
            if (localTagIds.indexOf(tagsClone[idx]) > -1) {
                continue;
            }
            localTagIds.push(tagsClone[idx]);
            tagsClone.splice(idx, 1);
        }
        // console.log('Tags, ', localTagIds);


        console.log(`Plant(${i}) ${plant.id} - add ${localTagIds.length} tags(s)`);
        // console.time('tags insert');
        for (let j = 0; j < localTagIds.length; j++) {
            //await session.create()
            let statement = session.create('EDGE', 'E').from(`SELECT FROM Plant WHERE id="${plant.id}"`).to(`SELECT FROM Tag WHERE id="${localTagIds[j]}"`);
            // console.log(statement.toString());
            await statement.one();
            // await session.query('CREATE EDGE E FROM (SELECT Plant WHERE id=:plantid) TO (SELECT Tag WHERE id=:tagid)', {
            //     params: {
            //         plantid: plant.id,
            //         tagid  : localTagIds[j]
            //     }
            // })
        }
        // console.timeEnd('tags insert');
        // console.time('cat insert');
        await (session.create('EDGE', 'E').from(`SELECT FROM Plant WHERE id="${plant.id}"`).to(`SELECT FROM Category WHERE id="${categories[_.random(0, categories.length - 1)]}"`)).one();
        // console.timeEnd('cat insert');
    }
    console.timeEnd('insert posts');


    await session.close();
    console.log('Session closed');

    await pool.close();
    console.log('Pool closed');

    await client.close();
    console.log('Client closed');

    process.exit(0);
};

run();